## Tchnologies used

# - NestJs

# - Postgres

# - typeORM

# - typescript

 framework TypeScript starter repository.

## Installation

```bash
$ yarn install or yarn


$ npm install
```

## Running the app

```bash


# development
$ yarn  start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Ibrahima Sow](https://gitlab.com/ibraweb)
- 
- Twitter - [@ibraweb](https://twitter.com/Ibra_sow1)

## License

Nest is [MIT licensed](LICENSE).
