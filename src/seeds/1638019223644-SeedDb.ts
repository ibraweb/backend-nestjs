import { MigrationInterface, QueryRunner } from 'typeorm';

export class SeedDb1638019223644 implements MigrationInterface {
  name = 'SeedDb1638019223644';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO tags (name) VALUES ('dragons'), ('coffee'), ('nestjs')`,
    );

    await queryRunner.query(
      //password is sow
      `INSERT INTO users (username, email, password) VALUES ('ibra', 'isow77@gmail.com', 
      '$2b$10$H69v8c0QcAKxOFEZXL5mFu21VrOihBUIeZl42o3xhTGDvSH3.WpjC')`,
    );

    await queryRunner.query(
      `INSERT INTO articles (slug, title, description, body, "tagList", "authorId") VALUES ('fisrt-article',
          'fist article title', 'firt article description', 'firr body', 'coffee,dragons', 1)`,
    );
  }

  public async down(): Promise<void> {
    console.log('');
  }
}
