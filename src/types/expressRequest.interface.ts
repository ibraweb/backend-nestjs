import { UserEntity } from '@app/user/user.entity';
import { Request } from 'express';
export interface Expressrequest extends Request {
  user?: UserEntity;
}
